window.addEventListener('DOMContentLoaded', function () {  
  const canvas = document.getElementById("renderCanvas");
  const engine = new BABYLON.Engine(canvas, true);
  const scene = new BABYLON.Scene(engine);
  const camera = new BABYLON.ArcRotateCamera("Camera",2.2,1.1,18, BABYLON.Vector3.Zero(), scene);
  camera.attachControl(canvas, true);
  const light = new BABYLON.DirectionalLight("dir01", new BABYLON.Vector3(6, -10, -10), scene);
  canvas.addEventListener("click",(event)=> {
   // We try to pick an object
   var pickResult = scene.pick(event.clientX, event.clientY);
});
  // Render
  var importedFile;
  // default path for model
  const defaultModelPath='BOX_Babylon.babylon';
	this.getImportFile = (value) => {
		console.log(value);
		value = value.split('\\');
		s = value[value.length - 1];
		showModel(s);
	};

	const showModel = (path = defaultModelPath) => {
		//console.log()
		BABYLON.SceneLoader.ImportMesh("", "/Bracket/new/", path, scene, (newMeshes, particleSystems, skeleton)=> {
		newMeshes.forEach((mesh) => {
			mesh.material = new BABYLON.StandardMaterial("Material", scene);
		});
      //newMeshes[6].material.diffuseColor = new BABYLON.Color3(0, 1, 0);
      newMeshes[2].material.diffuseColor = new BABYLON.Color3(0, 1, 0);
      //console.log( newMeshes[5].position)
		newMeshes[1].position.z = -8;
      newMeshes[0].position.z=8
      //newMeshes[1].material.diffuseColor = new BABYLON.Color3(0,1,0)
    });
  }
  this.stopFunc=()=>{
    engine.stopRenderLoop();
  }
  this.startFunc=()=>{
    engine.runRenderLoop(()=> {
        scene.render();
        let fpsLabel = document.getElementById("fpsLabel");
        fpsLabel.innerHTML = engine.getFps().toFixed() + " fps";
     });
  }
  // show fps
  engine.runRenderLoop(()=> {
      scene.render();
      let fpsLabel = document.getElementById("fpsLabel");
      fpsLabel.innerHTML = engine.getFps().toFixed() + " fps";
      
   });
    // Check if window size is changed
  window.addEventListener("resize", ()=> {
      engine.resize();
  });
  showModel();

  window.addEventListener("keypress",function(){
    engine.stopRenderLoop();
    console.log("b1b1b1b1b1b1b1b1b1b1b1b1b1b1b1b1b1b1b1b1bb1")
  })


  var timeout,stopRender=0;
  document.onmousemove = ()=>{
    clearTimeout(timeout);
    if(stopRender===1){
      engine.runRenderLoop(()=> {
        scene.render();
        let fpsLabel = document.getElementById("fpsLabel");
        fpsLabel.innerHTML = engine.getFps().toFixed() + " fps";
    });
      stopRender=0;
    }

    timeout = setTimeout(()=>{engine.stopRenderLoop();stopRender=1;console.log("jjj")}, 2000);
  }
})