
app.controller('gitCtrl', function ($http, $scope, $window) {
    var VendorPath;
    var anatomyPath;
    var relationPos;
    var poss;
    var obj;
    var anatomy;
    var relationType;
    var canvas = document.getElementById("renderCanvas");
    var engine = new BABYLON.Engine(canvas, true);
    var scene = new BABYLON.Scene(engine);
    var camera = new BABYLON.ArcRotateCamera("Camera",2.2,1.1,18, BABYLON.Vector3(0,0,0),scene); 
    camera.setPosition(new BABYLON.Vector3(0, 2, 45));
    camera.attachControl(canvas, true);
    // var path="BOX.babylon"
    var light1 = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(1, 1, 0), scene);
    var light2 = new BABYLON.PointLight("light2", new BABYLON.Vector3(0, 1, -1), scene);
    light2.position=camera.position
    engine.runRenderLoop(function () {
            scene.render();
    });

    window.addEventListener("resize", function () {
            engine.resize();
    });
    
    //defined anatomy Object and put the anatomy file path and the position for each anatoy
    var anatomyObj={
        "right maxillary central_incisor.babylon": {PositonX:3},
        "right maxillary latral_incisor.babylon": {PositonX:9},
        "right maxillary canine.babylon": {PositonX:15},
        "right maxillary first premolar.babylon": {PositonX:21},
        "right maxillary second premolar.babylon": {PositonX:27},
        "left maxillary central_incisor.babylon": {PositonX:-3},
        "left maxillary latral_incisor.babylon": {PositonX:-9},
        "left maxillary canine.babylon": {PositonX:-15},
        "left maxillary first premolar.babylon": {PositonX:-21},
        "left maxillary second premolar.babylon": {PositonX:-27},
    }

    //defined vendor Object and put the vendor file path
    var vendorObj={
        "Aligner CutoAppliance.babylon":true,
        "Aligner Cutout.babylon":true,
        "Aligner Elastic Cut.babylon":true,
        "Aligner.babylon":true,
        "Attachmet .babylon":true,
        "Band.babylon":true,
        "Bend.babylon":true,
        "BiteTurbo.babylon":true,
        "Bracket .babylon":true,
        "Bridge.babylon":true,
        "Button.babylon":true,
    }

    //submit function to add relation between anatomy and vendor object
    $scope.submit = function(vendor,relation,anatomy){
 
        console.log("submit data" , vendor,relation,anatomy)
        var obj = {};
        obj.venderName = vendor;
        obj.anatomyName = anatomy;
        obj.relation = relation;
        
        //post request to add the relation between anatomy and vendor object
        $http.post("https://orthobackend.azurewebsites.net/api/Home/relation" , obj)
        .then(function (res) {
        
        $http.post("https://orthobackend.azurewebsites.net/api/Home/relationAnatomy", obj)
        .then(function (res) {
        $scope.anatomyRelation = res.data;
        console.log( " $scope.anatomyRelation", $scope.anatomyRelation );
        });
        $http.post("https://orthobackend.azurewebsites.net/api/Home/relationVender", obj)
        .then(function (res) {
        $scope.VenderRelation = res.data;
        console.log( " $scope.VenderRelation", $scope.VenderRelation );
        });
        });

        //if the user add any anatomy or vendor object out of anatomyObj and vendorObj we return alert to tell user about that
        if(anatomyObj[anatomyPath] === undefined || vendorObj[VendorPath] === undefined){
            alert("Anatomy object or Vendor object not exists");
        }
        // if anatomy or vendor object inside of anatomyObj and vendorObj
        else{
                //the switch statment to add the anatomy and vendor for specific relation
                //we have 4 relation 
                switch (relationPos) {
                    case "A_top_of_B":

                    //import the anatomy object by anatomyPath 
                    //anatomyPath come from the click on specific anatomy from anatomy list
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/anatomy/", anatomyPath, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].material.diffuseColor = new BABYLON.Color3(0,1,0);
                            newMeshes[0].scaling={x:3,y:3,z:3}
                            console.log("anatomyPath   ",anatomyPath)
                            for (var key in anatomyObj) {
                                if(anatomyPath === key){
                                        newMeshes[0].position={x: anatomyObj[key]["PositonX"],y:0,z:0}
                                        poss= anatomyObj[key]["PositonX"]
                                }
                            }    
                        });

                         //import the vendor object by vendorPath 
                        //vendorPath come from the click on specific vendor from vendor list
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/vender/", VendorPath, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].position={x:poss,y:4,z:0}
                            newMeshes[0].scaling={x:2,y:2,z:2}
                        });
                    break;
                
                    case "A_Bottom_of_B":
                    
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/anatomy/", anatomyPath, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].material.diffuseColor = new BABYLON.Color3(0, 1, 0);
                            newMeshes[0].scaling={x:3,y:3,z:3}
                            for (var key in anatomyObj) {
                                if(anatomyPath === key){
                                    newMeshes[0].position={x: anatomyObj[key]["PositonX"],y:0,z:0}
                                    poss= anatomyObj[key]["PositonX"]
                                }
                            }
                        });
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/vender/", VendorPath, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].position={x:poss,y:-4,z:0}
                            newMeshes[0].scaling={x:2,y:2,z:2}
                        });
                    break;

                    case "A_Front_of_B":
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/anatomy/", anatomyPath, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].material.diffuseColor = new BABYLON.Color3(0, 1, 0);
                            newMeshes[0].scaling={x:3,y:3,z:3}
                            for (var key in anatomyObj) {
                                if(anatomyPath === key){
                                    newMeshes[0].position={x: anatomyObj[key]["PositonX"],y:0,z:0}
                                    poss= anatomyObj[key]["PositonX"]
                                }
                            }     
                        });
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/vender/", VendorPath, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].position={x:poss,y:0,z:4}
                            newMeshes[0].scaling={x:2,y:2,z:2}
                    
                        });
                    break;

                    case "A_Back_of_B":
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/anatomy/", anatomyPath, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].material.diffuseColor = new BABYLON.Color3(0, 1, 0);
                            newMeshes[0].scaling={x:3,y:3,z:3}
                            for (var key in anatomyObj) {
                                if(anatomyPath === key){
                                    newMeshes[0].position={x: anatomyObj[key]["PositonX"],y:0,z:0}
                                    poss= anatomyObj[key]["PositonX"]
                                }
                            }
                        });
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/vender/", VendorPath, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].position={x:poss,y:0,z:-4}
                            newMeshes[0].scaling={x:2,y:2,z:2}
                        });
                    break;
                }
            }
        }

    $scope.position = function(name){
        $scope.relationPosition = name;
        relationPos=name
        console.log(" $scope.relationPosition" ,  $scope.relationPosition);
        console.log("relationPos     ",relationPos)
        
    }
    

    $scope.link = function(name){
        $scope.linkRelation = name;
        console.log(" $scope.linkRelation " ,   $scope.linkRelation);   
    }
    
    $scope.getDetailVender = function (name) {
        console.log("++++++++++++++++++++++++++++++veeeendor" , name)
        $scope.venderDetails = name
        VendorPath=$scope.venderDetails.name+".babylon"
        var obj = {};
        obj.venderName = name.name;
        
        //send the VendorPath from the list of vendor to the database
        $http.post("https://orthobackend.azurewebsites.net/api/Home/relationVender", obj)
        .then(function (res) {
        console.log(res);
        $scope.VenderRelation = res.data;
        console.log( " $scope.VenderRelation", $scope.VenderRelation );
        });
        }

    $scope.getDetail = function (name) {
        console.log("++++++++++++++++++++++++++++++", name)
        $scope.anatomyDetails = name
        anatomyPath=$scope.anatomyDetails.name+".babylon"
        var obj = {};
        obj.anatomyName = name.name;

        //send the AnatomyPath from the list of anatomy to the database
        $http.post("https://orthobackend.azurewebsites.net/api/Home/relationAnatomy", obj)
        .then(function (res) {
        console.log(res);
        $scope.anatomyRelation = res.data;
        console.log( " $scope.anatomyRelation", $scope.anatomyRelation );
        });
        }
    
    // loadToTheScene function to load the last scene from database
    $scope.loadToTheScene = function(){
        var allDBRelation= $scope.allRelation
        for(i=0;i<allDBRelation.length; i++){
        anatomy=allDBRelation[i].anatomyname+".babylon"
        relationType=allDBRelation[i].type
        
        switch (allDBRelation[i].type) {
            case "A_top_of_B":
                for (key in anatomyObj) {
                    const xx= anatomyObj[key]["PositonX"]
                    let vendor=allDBRelation[i].vendorname+".babylon"
                    if(anatomy === key){
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/anatomy/", anatomy, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].material.diffuseColor = new BABYLON.Color3(0,1,0);
                            newMeshes[0].scaling={x:3,y:3,z:3}
                            newMeshes[0].position={x: xx,y:0,z:0}
                            BABYLON.SceneLoader.ImportMesh("", "app/objects/vender/", vendor, scene, function (newMeshes, particleSystems) {
                                newMeshes[0].position={x:xx,y:4,z:0}
                                newMeshes[0].scaling={x:2,y:2,z:2}
                            });
                        });
                    }
                }
            break;
           
            case "A_Bottom_of_B":
                for (var key in anatomyObj) {
                    poss= anatomyObj[key]["PositonX"]
                    let xx= anatomyObj[key]["PositonX"]
                    let vendor=allDBRelation[i].vendorname+".babylon"
                    if(anatomy === key){
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/anatomy/", anatomy, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].material.diffuseColor = new BABYLON.Color3(0, 1, 0);
                            newMeshes[0].scaling={x:3,y:3,z:3}
                            newMeshes[0].position={x: xx,y:0,z:0}
                            BABYLON.SceneLoader.ImportMesh("", "app/objects/vender/", vendor, scene, function (newMeshes, particleSystems) {
                                newMeshes[0].position={x:xx,y:-4,z:0}
                                newMeshes[0].scaling={x:2,y:2,z:2}
                            });
                    });
                    }
                }
            break;
            case "A_Front_of_B":
                for ( key in anatomyObj) {
                    let xx= anatomyObj[key]["PositonX"]
                    let vendor=allDBRelation[i].vendorname+".babylon"
                    if(anatomy === key){
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/anatomy/", anatomy, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].material.diffuseColor = new BABYLON.Color3(0, 1, 0);
                            newMeshes[0].scaling={x:3,y:3,z:3}
                            newMeshes[0].position={x: xx,y:0,z:0}
                            BABYLON.SceneLoader.ImportMesh("", "app/objects/vender/", vendor, scene, function (newMeshes, particleSystems) {
                                newMeshes[0].position={x:xx,y:0,z:4}
                                newMeshes[0].scaling={x:2,y:2,z:2}
                        
                            });
                        });
                    }          
                } 
            break;
            case "A_Back_of_B":
                for ( key in anatomyObj) {
                    let xx= anatomyObj[key]["PositonX"]
                    let vendor=allDBRelation[i].vendorname+".babylon"
                    if(anatomy === key){
                        BABYLON.SceneLoader.ImportMesh("", "app/objects/anatomy/", anatomy, scene, function (newMeshes, particleSystems) {
                            newMeshes[0].material.diffuseColor = new BABYLON.Color3(0, 1, 0);
                            newMeshes[0].scaling={x:3,y:3,z:3}
                            newMeshes[0].position={x: xx,y:0,z:0}
                            BABYLON.SceneLoader.ImportMesh("", "app/objects/vender/", vendor, scene, function (newMeshes, particleSystems) {
                                newMeshes[0].position={x:xx,y:0,z:-4}
                                newMeshes[0].scaling={x:2,y:2,z:2}
                        
                            });
                        });
                    }          
                }
            break;
        } 
        }  
    }
        
    //get all anatomy from database to show them in the anatomy list
    $http.get("https://orthobackend.azurewebsites.net/api/Home/Anatomy")

    .then(function (data) {
        $scope.Anatomy = data.data;
        console.log("About pageeeeeee", $scope.Anatomy)

    }).catch(function (error) {
        console.log(error);
    });

    //get all vendor from database to show them in the vendor list
    $http.get("https://orthobackend.azurewebsites.net/api/Home/Vender")

    .then(function (data) {
        $scope.Vender = data.data;

    }).catch(function (error) {
        console.log(error);
    });

//get all relation from database
    $http.get("https://orthobackend.azurewebsites.net/api/Home/allRelation")
    .then(function (data) {
    $scope.allRelation = data.data;
    console.log( "$scope.allRelation fro database", $scope.allRelation)
        
    }).catch(function (error) {
    console.log(error);
    });

 
   
})